<?php
namespace Core;

class Route
{


// Регистрация маршрутов
    const ROUTE = array(
        '/' => 'Controller_main@index',
        '/admin' => '/AdminControllers/Controller_Admin_main@index',
        '/admin/blog/create' => '/AdminControllers/Controller_Admin_blog@create',
        '/admin/blog/store' => '/AdminControllers/Controller_Admin_blog@store',

        '/blog' => 'Controller_blog@index',


        '/portfolio/{id}' => 'Controller_portfolio@Work', // В {} заключаем динамику
        '/portfolio' => 'Controller_portfolio@index', //

    );

    protected function getPath($path){
        $nPath = end(explode("/", $path));
        $names = explode("@", $nPath);
        $controller_name = $names[0];
        $action_name = $names[1];
        preg_match("#/(.*)/#", $path, $matches);

        $controller['path'] = $matches[1].'/';
        $controller['name'] = $controller_name;
        $controller['action'] = $action_name;

        return $controller;
    }

    protected function getUrl($request){
        $url = array();
        if (!self::ROUTE[$request]){
            $argument  = preg_replace('#[^/]+$#su', '{id}', $request);
            preg_match('#[^/]+$#su', $request, $id);

            if (self::ROUTE[$argument]) {
                $url['id'] = $id[0];
                $url['path'] = self::ROUTE[$argument];
            }
        }else{
            $url['path'] = self::ROUTE[$request];
        }

        self::router($url);
    }

    protected function router($rout){
        $cPath = self::getPath($rout['path']);
        $controller_path = 'application/controllers/'.$cPath['path'].$cPath['name'].'.php';


        if(file_exists($controller_path)){
            include_once $controller_path;
        }else{
            route::ErrorPage404();
        }

        $action_name = 'action_'.$cPath['action'];

        $model_name = str_replace('Controller', 'Model', $cPath['name']);
        $model_path = 'application/models/'.$model_name.'.php';


        if(file_exists($model_path)){
            include_once $model_path;
        }

        $controller = new $cPath['name'];

        if(method_exists($controller, $action_name)){
            if(isset($rout['id'])) {
                $controller->$action_name($rout['id']);
            }else{
                $controller->$action_name();
            }
        }else{
            route::ErrorPage404();
        }
    }


    static function start(){
        self::getUrl(mb_strtolower($_SERVER['REQUEST_URI']));
    }

    static function ErrorPage404(){
        require_once '/application/views/404.php';
        die();
    }
}