<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>404 Error | Triangle</title>
    <link href="/application/views/css/bootstrap.min.css" rel="stylesheet">
    <link href="/application/views/css/font-awesome.min.css" rel="stylesheet">
    <link href="/application/views/css/main.css" rel="stylesheet">
    <link href="/application/views/css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
    <section id="error-page">
        <div class="error-page-inner">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <div class="bg-404">
                                <div class="error-image">                                
                                    <img class="img-responsive" src="/application/views/images/404.png" alt="">
                                </div>
                            </div>
                            <h2>СТРАНИЦА НЕ НАЙДЕНА</h2>

                            <a href="index.html" class="btn btn-error">ПЕРЕЙТИ НА ГЛАВНУЮ</a>
                            <div class="social-link">
                                <span><a href="#"><i class="fa fa-facebook"></i></a></span>
                                <span><a href="#"><i class="fa fa-twitter"></i></a></span>
                                <span><a href="#"><i class="fa fa-google-plus"></i></a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    <script type="text/javascript" src="/application/views/js/jquery.js"></script>
    <script type="text/javascript" src="/application/views/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/application/views/js/wow.min.js"></script>
    <script type="text/javascript" src="/application/views/js/main.js"></script>
</body>
</html>