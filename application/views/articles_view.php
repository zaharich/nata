<h1>Страница Статей: </h1>


<ul>
	<?php if(isset($data['articles'])):?>
    
    <?php foreach($data['articles'] as $article_object => $article_str):?>
        <li>
            <h2><?=$article_str->title?></h2>
            
            <?=$article_str->text?> 
            <br>
            <a href="articles/article/<?=$article_str->id?>">Читать...</a>
        </li>
    <?php endforeach;?>
    
	<?php elseif(isset($data['article'])):?>
		<h2>Статья: №<?=$data['article']->id;?></h2>

    	<?=$data['article']->title;?>
    	
    	<br>
    	
    	<?=$data['article']->text;?>
    <?php endif;?>

</ul>