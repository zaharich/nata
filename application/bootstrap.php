<?php
require_once 'config.php';

use Core\route;


spl_autoload_register ('autoload');
function autoload ($className) {
    $fileName = $className.'.php';
    include  str_replace('\\','/',$fileName);
}

Route::start();