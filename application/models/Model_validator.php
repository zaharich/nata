<?php

namespace models;

class Validator
{

    public $post = array();

    public function required($data){
        $data = trim($data);
        if($data != NULL){
            return $data;
        }else{
            return 'Required';
        }
    }
    public function clean($data){
        $data = stripslashes($data);
        $data = strip_tags($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function validate_rules(array $post,array $rulesArr){ // передаем массив например $_POST и массив с правилами
        foreach ($post as $field => $value){

                $rules = explode('|',$rulesArr[$field]); //Получаем имена методов проверки

                foreach ($rules as $rule){ //Проходим по массиву
                    if ($rule == 'notclean'){ //Если не хотим чистку то в свойство передаем без измен
                        $this->post[$field] = $value;
                    }else{
                        $value = $this->clean($value); //Чистим ввод
                        if($rulesArr[$field]){ //Если существует такой индекс
                            $this->post[$field] = $this->$rule($value); //Вызываем метод из правил
                        }else{
                            $this->post[$field] = $value;
                        }
                    }
                }

        }
    }
}