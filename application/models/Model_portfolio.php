<?php
use Core\Model;

class Model_Portfolio extends Model
{
    public function getWorks()
    {

    	$query = "SELECT * FROM portfolio WHERE id>=1";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
	    $rows = $stmt->fetchAll(PDO::FETCH_CLASS);

        return $rows;
    }
    public function getOneWork($arg)
    {

    	$query = "SELECT * FROM portfolio WHERE id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->execute(array(':id' => $arg));
	    $rows = $stmt->fetchObject();

        return $rows;
    }
}