<?php
class Model_Articles extends Model
{
	    public function getArticles()
    {

    	$query = "SELECT * FROM articles WHERE id>=1";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
	    $rows = $stmt->fetchAll(PDO::FETCH_CLASS);

        return $rows;
    }

        public function getOneArticle($arg)
    {

    	$query = "SELECT * FROM articles WHERE id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->execute(array(':id' => $arg));
	    $rows = $stmt->fetchObject();
	    
        return $rows;
    }
}