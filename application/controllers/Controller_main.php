<?php
use Core\Controller;

class Controller_Main extends Controller
{

    public function action_index(){
    $data['title'] = 'HELLO i title';

        $this->view->generate('main_view.php','template_view.php',$data);
    }

    public function action_aboutUs(){
        $this->view->generate('aboutUs_view.php','template_view.php');
    }

    public function action_test()
    {
    	$this->view->generate('test_view.php','template_view.php');
    }
}