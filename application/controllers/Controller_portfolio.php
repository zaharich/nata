<?php

use Core\Controller;


class Controller_Portfolio extends Controller
{
    public function action_index()
    {
       $portfolioModel = new Model_Portfolio();
       $data['works'] = $portfolioModel->getWorks();

        $this->view->generate('portfolio_view.php','template_view.php', $data);
    }
    public function action_Work($arg)
    {
       $this->checkInt($arg);
       $portfolioModel = new Model_Portfolio();
       $data['work'] = $portfolioModel->getOneWork($arg);

        $this->view->generate('portfolio_view.php','template_view.php', $data);
    }
}