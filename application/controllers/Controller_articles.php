<?php
use Core\Controller;

class Controller_Articles extends Controller
{
	public function action_index()
    {
       $portfolioModel = new Model_Articles();
       $data['articles'] = $portfolioModel->getArticles();

        $this->view->generate('articles_view.php','template_view.php', $data);
    }

     public function action_Article($arg)
    {
       $this->checkInt($arg);
       $portfolioModel = new Model_Articles();
       $data['article'] = $portfolioModel->getOneArticle($arg);

          $this->view->generate('articles_view.php','template_view.php', $data);
    }

}