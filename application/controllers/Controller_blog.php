<?php

use Core\Controller;

class Controller_Blog extends Controller{

    public function action_index()
    {
        $portfolioModel = new Model_Blog();
        $data['works'] = $portfolioModel->getWorks();

        $this->view->generate('blog/blog_view.php','template_view.php');
    }

    public function action_create(){

    }
}