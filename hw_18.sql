-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 22 2017 г., 16:08
-- Версия сервера: 5.5.53
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `hw_18`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `text`) VALUES
(1, 'Шахтерская байка', 'Во время обеденного перерыва в шахте наверх не вылазит никто. Обедают там же. И один шахтер после обеда прикорнул и заснул. Те, кто с ним работали в одной смене, должны были его после окончания перерыва разбудить — таков был уговор. Но они куда-то запропастились на несколько минут, но это неважно. Просыпается мужик от того, что кто-то его кусает за палец. Не до крови, но очень неприятно. И спать не дает. Открывает глаза — крыса! Он ее отогнал с помощью рук и матерных слов. Только снова закрыл глаза, опять кусает, зараза. Он и кидался в нее породой, и чего только не делал — не отстает. Плюнул, поднялся и погнался за ней по штреку. Через полминуты там, где он лежал, все завалило нафиг. Обрушилось перекрытие. После этого случая каждый раз, когда мужик приходит на работу и обедает, к нему приходит крыса, та самая. Он отрывает треть своего тормозка и дает ей. Сидят вместе едят. Поели, она встала и ушла. И так каждый раз. Откуда она узнает, когда у него следующая смена, никто понять не может...'),
(2, 'Театр!', 'В театре идет спектакль: \r\n- Жениться нужно!!! Не жениться мне нельзя... Во-первых, мне уже 35 лет — возраст, так сказать, критический. Главное — нужно решиться. Если же долго думать, колебаться, много разговаривать да ждать идеала или настоящей любви, то этак никогда не женишься...\r\n\r\nТут в партере раздается громкий голос девушки, обращенный своему кавалеру: - ПОНЯЛ?!! \r\nИ весь зал разразился смехом минуты на три. \r\nЗа весь спектакль, эта реплика имела самый большой успех. ');

-- --------------------------------------------------------

--
-- Структура таблицы `portfolio`
--

CREATE TABLE `portfolio` (
  `id` int(11) NOT NULL,
  `year` year(4) NOT NULL,
  `description` text NOT NULL,
  `url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `portfolio`
--

INSERT INTO `portfolio` (`id`, `year`, `description`, `url`) VALUES
(1, 2017, '<b>Цель:</b> Сделать магазинчик для своей любимой!!!<br>\r\n<b>Достижения:</b> Прикольный движок, но все таки не реализует модель MVC!!!', 'http://kreker.com.ua/'),
(2, 2011, '<b>Цель:</b> Сайт по метало обработке!!\r\n<br>\r\n<b>Достижения:</b> Занимает по некоторым ВЧ запросам топ!!!\r\n<br>\r\n<b>Не понравилось:</b> WordPress + делал на скорую руку.', 'http://zaharich.com.ua/');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `portfolio`
--
ALTER TABLE `portfolio`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `portfolio`
--
ALTER TABLE `portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
